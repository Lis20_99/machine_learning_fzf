#include <iostream>
#include <time.h>
#include <MyANN.h>

using namespace std;
using namespace ANN;

int main()
{
	setlocale(LC_ALL, "Russian");
	//cout << "�������� ������!" << endl;
	//cout << GetTestString().c_str() << endl;
	vector<vector<float>> inputs, outputs;
	vector<size_t> con = { 2, 10, 10, 1 };
	shared_ptr<ANeuralNetwork> neuron;
	// � LoadData � Save ������� filepath

	bool _loaded_Data = LoadData("..\\Debug\\Data.txt", inputs, outputs);
	switch (_loaded_Data)
	{
	case true:
		cout << "������ ������� ���������" << endl;
		neuron = CreateNeuralNetwork(con);
		srand(time(0));
		neuron->RandomInit();
		BackPropTraining(neuron, inputs, outputs, 10000.f, 0.1, 0.1f, true);
		cout << "\n" << neuron->GetType() << endl;
		switch (neuron->Save("..\\Debug\\XOR.anc"))
		{
		case true:
			cout << "��������� ���� ������� ���������!" << endl;
			break;
		case false:
			cout << "������!	��������� ���� �� ���������!" << endl;
			break;
		}
		break;
	case false:
		cout << "������!	������ �� ���������" << endl;
		break;
	}
	system("pause");
	return 0;
}