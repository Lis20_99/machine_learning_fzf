#include <iostream>
#include <MyANN.h>
using namespace std;
using namespace ANN;

int main()
{
	setlocale(LC_ALL, "Russian");
	//cout << GetTestString().c_str() << endl;
	vector<vector<float>> in, out;
	vector<float> res;
	vector<size_t> con = { 2, 10, 10, 1 }; // ��� ����� ��������, ����� ����� ������
	shared_ptr<ANeuralNetwork> neuron = CreateNeuralNetwork(con);
	bool _loaded_ann = neuron->Load("..\\Debug\\XOR.anc");
	switch (_loaded_ann)
	{
	case true:
		cout << "��������� ���� ���������!" << endl;
		cout << neuron->GetType() << endl;
		switch (LoadData("..\\Debug\\Data.txt", in, out))
		{
		case true:
			cout << "������ ������� ���������" << endl;
			for (int i = 0; i < in.size(); i++)
			{
				res = neuron->Predict(in[i]);
				cout << "�����: ";
				for (int j = 0; j < in[i].size(); j++)
				{
					cout << in[i][j];
					if (j != (in[i].size() - 1)) cout << ", ";
					else cout << "|\t";
				}
				cout << "�����: ";
				for (int j = 0; j < res.size(); j++)
					cout << res[j] << endl;
			}
			break;
		case false:
			cout << "������!	������ �� ���������" << endl;
			break;
		}
		break;
	case false:
		cout << "������!	��������� ���� �� ���������!" << endl;
		break;
	}
	system("pause");
	return 0;
}